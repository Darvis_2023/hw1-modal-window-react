import React, {Component} from 'react';

class ProductCard extends Component {
    render() {
        return (
            <div className="product-block">
                <div className="product-block__img">
                    <a href="" className="product-block__img-link">
                        <img src="" alt=""/>
                    </a>
                </div>

            </div>

        );
    }
}

export default ProductCard;